import axios from 'axios';

export default class Kittens {

    static fetchAll() {
        return axios.get('api/kittens');
    }
}
