import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import kittens from '../api/kittens';

export function createStore () {
    return new Vuex.Store({
        state: {
            kittens: [],
        },
        actions: {
            fetchAll ({ commit }) {
                return kittens.fetchAll().then(response => {
                    commit('setKittens', { data: response.data });
                })
            },
        },
        mutations: {
            setKittens (state, { data }) {
                state.kittens = data;
            }
        },
        getters: {
            kittens: state => {
                return state.kittens;
            },
        }
    });
}
