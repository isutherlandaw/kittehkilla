import App from './components/App.vue';
import Vue from 'vue';

import { createStore } from './store/store';

const store = createStore();

export default new Vue({
    store,
    render: h => h(App)
});
