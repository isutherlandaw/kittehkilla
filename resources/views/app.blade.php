<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vue/Laravel SSR App</title>
    <style>
        .dead {
            text-decoration: line-through;
        }
        .big-red-button {
            width: 200px;
            height: 200px;
            background: red;
            border: solid 2px red;
            color: white;
            border-radius: 100%;
            font-size: 30px;
            font-weight: bold;
            cursor: pointer;
        }

        .big-red-button:focus {
            outline: none;
        }
    </style>
</head>
<body>
    {!! $ssr !!}
    <script src="{{ asset('js/entry-client.js') }}" type="text/javascript"></script>
</body>
</html>
