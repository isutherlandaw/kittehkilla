<?php

namespace App\Http\Controllers;


class ApiController extends Controller
{
    public function kittens() {
        return response()->json([
            ['name' => 'Furry', 'dead' => false],
            ['name' => 'Cuddles','dead' => false],
            ['name' => 'Cutie','dead' => false],
            ['name' => 'Mogwai','dead' => false],
            ['name' => 'PussPuss','dead' => false],
            ['name' => 'Grumpy','dead' => false],
        ]);
    }
}
