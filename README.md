Example of server-side rendering using Laravel and Vue.js 2.5.

To see an example including Vue Router, check out [this repo](https://github.com/anthonygore/vue-js-laravel-multi-ssr).

Requires the [php-v8js](https://github.com/phpv8/v8js) extension to be installed in your environment.

Uses [jerev/docker-php-apache-v8js](https://hub.docker.com/r/jerev/docker-php-apache-v8js/) as a docker container and [node](https://hub.docker.com/_/node/) for the js build

Clone the repo
run 

`
./bin/build
`
 App should be available on http://localhost (uses port 80)
 
 If there is a problem (eg: unauthorised to view) try killing the container and starting it from a shell
 
 `
 docker stop $(docker ps -aq) && docker rm -f $(docker ps -aq) && docker-compose up -d server
 `
